let paused: boolean = false
let img: p5.Image;
let grid: { a: number, b: number }[][] = []

// define constants used in the reaction diffusion algorithm
let dA = 1
let dB = 0.5
let feed = 0.055
let kill = 0.062

function preload() {
  img = loadImage('img/rain.jpeg');
}

function setup() {
  setupScreen()

  // crop image so it's the same size as the frame
  let croppedImage: p5.Image = createImage(width, height)
  if (img.width / img.height <= width / height) {
    // higher than frame
    img.resize(width, 0)
    croppedImage.copy(img, 0, round(img.height / 2 - height / 2), width, height, 0, 0, width, height)
  } else {
    // wider than frame
    img.resize(0, height)
    croppedImage.copy(img, round(img.width / 2 - width / 2), 0, width, height, 0, 0, width, height)
  }
  img = croppedImage

  img.loadPixels()
  let d = pixelDensity()
  for (let x = 0; x < width; x++) {
    grid[x] = []
    for (let y = 0; y < height; y++) {
      let index = 4 * (y * width * d + x * d)

      grid[x][y] = {
        a: 1,
        b: 1 - (img.pixels[index] + img.pixels[index + 1] + img.pixels[index + 2]) / 3 / 255
      }
    }
  }

  // TODO: remove this
  pause()
}

function draw() {
  background(0)

  updateGrid()

  let d = pixelDensity()
  loadPixels()
  for (let x = 0; x < width; x++) {
    for (let y = 0; y < height; y++) {
      let index = 4 * (y * width * d + x * d)
      let intensity = floor((grid[x][y].a - grid[x][y].b) * 255)
      pixels[index] = intensity
      pixels[index + 1] = intensity
      pixels[index + 2] = intensity
      pixels[index + 3] = 255
    }
  }

  updatePixels()
}

function updateGrid() {
  let next: { a: number, b: number }[][] = []

  for (let x = 0; x < grid.length; x++) {
    next[x] = []
    for (let y = 0; y < grid[x].length; y++) {
      const a = grid[x][y].a
      const b = grid[x][y].b

      // calculate a and b for the next cycle
      let nextA = a + (dA * laplace('a', x, y)) - (a * b * b) + (feed * (1 - a))
      nextA = constrain(nextA, 0, 1)
      let nextB = b + (dB * laplace('b', x, y)) + (a * b * b) - ((kill + feed) * b)
      nextB = constrain(nextB, 0, 1)

      next[x][y] = {a: nextA, b: nextB}
    }
  }

  grid = next
}

function laplace(property: 'a' | 'b', x: number, y: number): number {
  let sum = 0
  sum += grid[x][y][property] * -1

  if (x > 0) {
    sum += grid[x - 1][y][property] * .2
    if (y > 0)
      sum += grid[x - 1][y - 1][property] * .05
    if (y < height - 1)
      sum += grid[x - 1][y + 1][property] * .05
  }

  if (x < width - 1) {
    sum += grid[x + 1][y][property] * .2
    if (y > 0)
      sum += grid[x + 1][y - 1][property] * .05
    if (y < height - 1)
      sum += grid[x + 1][y + 1][property] * .05
  }

  if (y > 0)
    sum += grid[x][y - 1][property] * .2

  if (y < height - 1)
    sum += grid[x][y + 1][property] * .2

  return sum
}

/**
 * p5 builtin function to handle window resize events
 */
function windowResized() {
  setupScreen()
}

function setupScreen() {
  createCanvas(windowWidth, windowHeight)
}

function pause() {
  paused = true
  noLoop()
  console.log('loop paused')
}

function unpause() {
  paused = false
  loop()
  console.log('resuming loop')
}

function toggle_pause() {
  if (paused)
    unpause()
  else
    pause()
}

/**
 * p5 builtin function to handle key press events
 */
function keyPressed() {
  if (keyCode === ESCAPE)
    pause()
  else if (key === ' ')
    toggle_pause()
}

function mouseClicked() {
  // TODO: handle mouse clicks
}
