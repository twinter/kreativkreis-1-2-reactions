# Reactions

A project by Tom Winter during Phase 2 of the first edition of the
"KreativKreis" project.

The project lives here:
<https://gitlab.com/twinter/kreativkreis-1-2-reactions>.

This is heavily based on this article by Karl Sims:
<http://www.karlsims.com/rd.html> and this this challenge by Daniel
Shiffman:
<https://thecodingtrain.com/CodingChallenges/013-reactiondiffusion-p5.html>.

## controls

Pause the loop with ESC or space, unpause with space.

## running it

1. install npm
2. run `npm install`
3. run `npm run start`

## Licenses

see LICENSE.txt

In short: this project as well as all included code fragments are licensed under
the MIT license.
