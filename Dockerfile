FROM node:latest

RUN mkdir /code
COPY package.json package.json
COPY package-lock.json package-lock.json
RUN npm ci

WORKDIR /code
